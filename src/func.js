const getSum = (str1, str2) => {
  if(typeof str1 === 'object' && typeof str2 === 'object'){
    return false;
  }else if(/[A-Za-z]/.test(str1)||/[A-Za-z]/.test(str2)){
    return false;
  }else{
    if(str1 === ''){
      str1 = 0
    }else if(str2 === ''){
      str2 = 0
    }
  }
  str1 = Number(str1)
  str2 = Number(str2)
  let str = str1 + str2
  return String(str);
};




const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let p = 0;
  let k = 0;
  for (const post of listOfPosts) {
    if(post.author === authorName){
      p++;
    }
    if(post.comments){
      for(const comment of post.comments){
        if(comment.author === authorName){
          k++
        }
      }
    }
  }
  return `Post:${p},comments:${k}`;
};





const tickets=(people)=> {
  let hungred = 0;
  let fifty = 0;
  let twentyFive = 0;
  for (let ticket of people) {
    ticket = Number(ticket)
    if(ticket === 25){
      twentyFive++;
    }else if(ticket === 50){
      if(twentyFive>0){
        twentyFive--;
        fifty++;
      }else return 'NO'
    }else if(ticket === 100){
      if(twentyFive>0&&fifty>0){
        twentyFive--
        fifty--
        hungred++
      }else if(twentyFive>2){
        twentyFive = twentyFive-2
        hungred++
      }else{
        return 'NO'
      }
    }else if(ticket!==25 && ticket!==50 && ticket!==100) return 'NO'
  }
  return 'YES';
};
console.log(tickets([25, 50, 100]));
console.log(tickets([25, 50, 25, 25, 100]));
console.log(tickets([25, 50, 25, 50, 25, 50, 25, 100]));
console.log(tickets([35, 25, 50, 100]));
console.log(tickets([25, 25, 50, 50, 25, 100]));
console.log(tickets([100,25,50]));


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
